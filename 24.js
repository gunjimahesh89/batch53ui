function printDate() {
    const currentDate = new Date();
    const day = currentDate.getDate();
    const month = currentDate.getMonth() + 1; // Adding 1 because months are 0-based (January is 0)
    const year = currentDate.getFullYear();
  
    let monthText;
  
    switch (month) {
      case 1:
        monthText = "January";
        break;
      case 2:
        monthText = "February";
        break;
      case 3:
        monthText = "March";
        break;
      case 4:
        monthText = "April";
        break;
      case 5:
        monthText = "May";
        break;
      case 6:
        monthText = "June";
        break;
      case 7:
        monthText = "July";
        break;
      case 8:
        monthText = "August";
        break;
      case 9:
        monthText = "September";
        break;
      case 10:
        monthText = "October";
        break;
      case 11:
        monthText = "November";
        break;
      case 12:
        monthText = "December";
        break;
      default:
        monthText = "Invalid month";
        break;
    }
  
    console.log(`Todayis ${monthText} ${day}, ${year}`);
  }
  
  printDate();

  //map
const numbers=[1,2,3,4,5];
const cube =(num) => num **3;
const cubedNumbers=numbers.map(cube);
console.log(cubedNumbers);

//product
const numbers1 = [1, 2, 3, 4, 5];
const product = numbers1.reduce((product, num) => product + num, 0);
console.log(product);

//map filter even numbers
const numbers2 = [1, 2, 3, 4, 5];
const filter=numbers2.filter((num) => num%2 ===0);
console.log(filter);

//filter
const fruits=["mango","banana","guava","apple"]
const len=fruits.filter((num)=> num.length <= 6)
console.log(len);